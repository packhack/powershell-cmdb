# Simple SQLite CMDB Database for Powershell

Simple SQLite Powershell Module for inserting Commands to Servers and Actions.

## Usage

1. Enter Paths in the Module and provide SQLite Module (NetFX Module - https://system.data.sqlite.org/index.html/doc/trunk/www/downloads.wiki)
2. Include in your Profile (import-module $PATH\sqlute-cmdb.psm1)
3. In your Profile also include piInitDB for initialize the Datbase

## Use for personal

When you change something on Servers just do:

~~~
$srv = "server.name.fqdn"
$cmd = "net stop spooler"
invoke-command -computername $srv -scriptblock { ${cmd} }
piEnterCommand -cmdbServer $srv -cmdbCommand $cmd
~~~

Get the History:

~~~
piGetHistory -cmdbServer "server.name.fqdn"
~~~

## ToDo

* Execute Command on piEnterCommand
* Add User to Table History
