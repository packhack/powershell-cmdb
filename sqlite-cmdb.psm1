add-type -path system.data.sqlite.dll
$dbFile = "pisql.db"
$sqlConn = "data source=${dbFile}"

function piInitDB {
    if (-not(test-path -path $dbFile))
    {
        $connObj = New-Object -typename System.Data.SQLite.SQLiteConnection
        $connObj.ConnectionString = $sqlConn

        $sqlCommands = "create table computers (
            id integer PRIMARY KEY AUTOINCREMENT, fqdn varchar(255), ip varchar(13)
            )", "create table history (
                hid integer PRIMARY KEY AUTOINCREMENT, server_id integer, command text not null, command_date text
            )"
        $connObj.Open()
        $sqlStatement = $connObj.CreateCommand()
    
        foreach ($sqlCommand in $sqlCommands)
        {
            $sqlStatement.CommandText = $sqlCommand
            $sqlStatement.ExecuteNonQuery()
        }

        $connObj.Dispose()
        $sqlStatement.Dispose()
    }
}

function piEnterSql($sqlStatement)
{
    $connObj = New-Object -typename System.Data.SQLite.SQLiteConnection
    $connObj.ConnectionString = $sqlConn
    $connObj.open()

    $sqlCmd = $connObj.CreateCommand()
    $sqlCmd.CommandText($sqlStatement)
    $sqlCmd.ExecuteNonQuery()
    $sqlCmd.Dispose()
    $connObj.Dispose()
}

function piEnterCommand
{
    param(
        [string]$cmdbServer,
        [string]$cmdbCommand
    )
    $connObj = New-Object -typename System.Data.SQLite.SQLiteConnection
    $connObj.ConnectionString = $sqlConn
    $connObj.open()

    $sqlCmd = $connObj.CreateCommand()

    $sqlCmd.CommandText = "SELECT id FROM computers WHERE fqdn = '${cmdbServer}'"

    if (!($sqlCmd.ExecuteScalar()))
    {
        $computerIP = [System.Net.Dns]::GetHostByName(${cmdbServer}).AddressList.IPAddressToString
        $sqlCmd.CommandText = "INSERT INTO computers(fqdn,ip) VALUES ('${cmdbServer}','${computerIP}')"
        $sqlCmd.ExecuteNonQuery()
        
    }
    
    $getID = "SELECT id FROM computers WHERE fqdn = '${cmdbServer}'"
    $sqlCmd.CommandText = $getID
    $srvID = $sqlCmd.ExecuteScalar()
    $curDate = Get-Date -Format "yyyy-MM-dd HH:m:s"

    $sqlCmd.CommandText = "INSERT INTO history(server_id,command,command_date) values ( '${srvID}', '${cmdbCommand}', '${curDate}' )"
    $sqlCmd.ExecuteNonQuery()
    $sqlCmd.Dispose()
    $connObj.Dispose()
}

function piGetHistory
{
    param(
        [parameter(Mandatory)]
        [string]$cmdbServer
    )
    $connObj = New-Object -typename System.Data.SQLite.SQLiteConnection
    $connObj.ConnectionString = $sqlConn
    $connObj.open()

    $sqlCmd = $connObj.CreateCommand()
    $sqlCmd.CommandText = "SELECT id FROM computers where fqdn = '${cmdbServer}'"
    $srvID = $sqlCmd.ExecuteScalar()
    $sqlCmd.CommandText = "SELECT command,command_date FROM history WHERE server_id = '${srvID}'"
    $results = $sqlCmd.ExecuteReader()

    $row = @()
    while ($results.Read())
    {
        $result = "" | Select command,date
        $result.command = $results["command"]
        $result.date = $results["command_date"]
        $row += $result
    }
    $row
    $sqlCmd.Dispose()
    $connObj.Dispose()
}
